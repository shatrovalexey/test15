<?php
	/**
	* Поиск в яндекс
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* необходимы пакеты ОС:
	* lynx
	* translate-bin libtranslate0
	*/

	/**
	* @var $query string - поисковый запрос
	* @var $url string - URL запроса к yandex.ru
	* @var $inp_fh resource - дескриптор открытия pipe к lynx
	* Так как yandex всячески защищается от сторонних запросов к своему поиску,
	* для получения кода придётся использовать полноценный браузер. Например, lynx.
	*/

	$query = 'Поисковый запрос' ;
	$url = 'https://yandex.ru/?q=' . urlencode( $query ) ;
	$inp_fh = popen( 'lynx -source "' . quotemeta( $url ) . '"' ) ;

	if ( empty( $inp_fh ) ) {
		die( 'Не удалось получить содержимое ' . $url ) ;
	}

	/**
	* @var $before string - код до поисковой строки вместе с кодом поисковой строки
	* @var $after string - код после поисковой строки
	* @var $line string - очередная прочитанная строка
	*/
	$before = $after = $line = '' ;

	/**
	* Чтение исходного кода страницы и сохранение в переменные $before и $after до нахождения поисковой строки.
	*/
	while ( $line = fgets( $inp_fh ) ) {
		if ( preg_match( '{^(.*?<input\b[^>]+?class="input__control"[^>]*?>)(.*)$}is' , $line , $matches ) ) {
			$before .= $matches[ 1 ] ;
			$after .= $matches[ 2 ] ;

			break ;
		}

		$before .= $line ;
	}

	/**
	* Чтение остатков кода страницы в $after.
	*/
	while ( $line = fgets( $inp_fh ) ) {
		$after .= $line ;
	}

	/**
	* Закрытие дескриптора.
	*/
	fclose( $inp_fh ) ;

	/**
	* Опционально, перевод $after с русского на английский.
	*/
	if ( $translate_fh = popen( 'translate-bin -sgoogle -fru -ten' ) ) {
		error_log( 'Не удалось использовать переводчик' ) ;

		/**
		* Отправка $after в STDIN translate-bin
		*/
		fputs( $translate_fh , $after ) ;

		/**
		* Чтение перевода в $after.
		*/
		$after = '' ;
		while ( fread( $translate_fh , $line , 0x400 ) ) {
			$after .= $line ;
		}

		/**
		* Закрытие дескриптора.
		*/
		fclose( $translate_fh ) ;
	}

	/**
	* @const CHARSET string - кодировка по-умолчанию
	* @const XML_DECL string - декларация PI XML
	*/
	define( 'CHARSET' , 'utf-8' ) ;
	define( 'XML_DECL' , '<?xml version="1.0" encoding="' . CHARSET . '"?>' ) ;

	/**
	* @var $domh DOMDocument - объект DOM.
	* загрузка содержимого в память в виде дерева DOM.
	*/
	$domh = new DOMDocument( '1.0' , CHARSET ) ;
	@$domh->loadHTML( XML_DECL . $before . $after ) ;

	/**
	* $before, $after и $line больше не нужны.
	*/
	unset( $before ) ;
	unset( $after ) ;
	unset( $line ) ;

	/**
	* @var $xpathh DOMXPath - объект DOMXPath.
	*/
	$xpathh = new DOMXPath( $domh ) ;

	/**
	* Перемешивание пунктов вывода
	* @var $i int - счётчик
	* @var $list DOMNode - очередной элемент DOM списка результатов поиска
	*/
	foreach ( $xpathh->query( '//ul[@class="serp-list serp-list_left_yes"]' ) as $i => $list ) {
		/**
		* @var $item_list array - список клонов элементов списка $list
		*/
		$item_list = array( ) ;

		/**
		* Поиск, клонирование и удаление элементов списка $list.
		*/
		foreach ( $xpathh->query( 'li[@class="serp-item t-construct-adapter__legacy"]' , $list ) as $j => $item ) {
			$item_list[] = $item->clone( true ) ;
			$item->parentNode->removeChild( $item ) ;
		}

		/**
		* Перемешивание пунктов $item_list
		*/
		shuffle( $item_list ) ;

		/**
		* Запись пунктов в $list
		*/
		foreach ( $item_list as $j => $item ) {
			$list->appendChild( $item ) ;
		}
	}

	/**
	* @var $contents string - исходный текст документа
	*/
	$contents = $domh->saveHTML( ) ;
	$contents = preg_replace( '{^\s*' . preg_quote( XML_DECL ) . '}is' , '' , $contents ) ;

	/**
	* вывод результата
	*/
	echo $contents ;